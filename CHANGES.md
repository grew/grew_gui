## 1.3.4 (2020/10/02)
  * follow libcaml-grew 1.4

## 1.3.3 (2020/05/05)
  * add Amr dependence

## 1.3.2 (2020/05/05)
  * add Amr support

## 1.3.1 (2020/01/18)
  * remove Pervasives deprecated ocaml lib

# 1.3.0 (2019/06/24)
  * Follow libcaml-grew versioning
  * Fix: Uncaught exception

# 1.2.0 (2019/03/26)
  * Follow libcaml-grew versioning

## 1.1.1 (2019/03/26)
  * Move to opam2

# 1.1.0 (2018/11/23)
  * Follow libcaml-grew versioning

# 1.0.0 (2018/09/10)
  * Improve error handling
  * Follow libcaml-grew versioning

## 0.49.0 (2018/07/05)
  * Follow libcaml-grew versioning

## 0.48.0 (2018/06/05)
  * Follow libcaml-grew versioning

### 0.47.1 (2018/05/22)
 * Remove lablgladecc2 from dependencies

## 0.47.0 (2018/03/13)
 * adapt to new libcaml-grew

### 0.46.3 (2018/01/17)
 * Fix automatic scrolling (and remove event_box, contextual menu is no more available on graphs)

### 0.46.2 (2018/01/16)
 * Fix a few bug with GUI (error not visible and active SVG problem)

### 0.46.1 (2018/01/03)
 * Adapt to libcaml_dep2pict 2.30
 * add Conll error in handling function

## 0.46.0 (2017/12/14)
Bump version number to synchronize

## 0.45.0 (2017/10/10)
First version of the GTK interface as an independent executable: grew_gui
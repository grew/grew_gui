(* Automatically generated from src/grew.glade by lablgladecc *)

class config_window ?(file="src/grew.glade") ?domain ?autoconnect(*=true*) () =
  let xmldata = Glade.create ~file  ~root:"config_window" ?domain () in
  object (self)
    inherit Glade.xml ?autoconnect xmldata
    val toplevel =
      new GWindow.window (GtkWindow.Window.cast
        (Glade.get_widget_msg ~name:"config_window" ~info:"GtkWindow" xmldata))
    method toplevel = toplevel
    val config_window =
      new GWindow.window (GtkWindow.Window.cast
        (Glade.get_widget_msg ~name:"config_window" ~info:"GtkWindow" xmldata))
    method config_window = config_window
    val config_vbox =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"config_vbox" ~info:"GtkVBox" xmldata))
    method config_vbox = config_vbox
    val hbox20 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox20" ~info:"GtkHBox" xmldata))
    method hbox20 = hbox20
    val label4 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label4" ~info:"GtkLabel" xmldata))
    method label4 = label4
    val main_feat =
      new GEdit.entry (GtkEdit.Entry.cast
        (Glade.get_widget_msg ~name:"main_feat" ~info:"GtkEntry" xmldata))
    method main_feat = main_feat
    val label_features =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label_features" ~info:"GtkLabel" xmldata))
    method label_features = label_features
    val btn_config_close =
      new GButton.button (GtkButton.Button.cast
        (Glade.get_widget_msg ~name:"btn_config_close" ~info:"GtkButton" xmldata))
    method btn_config_close = btn_config_close
    method reparent parent =
      config_vbox#misc#reparent parent;
      toplevel#destroy ()
    method check_widgets () = ()
  end
class grew_window ?(file="src/grew.glade") ?domain ?autoconnect(*=true*) () =
  let xmldata = Glade.create ~file  ~root:"grew_window" ?domain () in
  object (self)
    inherit Glade.xml ?autoconnect xmldata
    val toplevel =
      new GWindow.window (GtkWindow.Window.cast
        (Glade.get_widget_msg ~name:"grew_window" ~info:"GtkWindow" xmldata))
    method toplevel = toplevel
    val grew_window =
      new GWindow.window (GtkWindow.Window.cast
        (Glade.get_widget_msg ~name:"grew_window" ~info:"GtkWindow" xmldata))
    method grew_window = grew_window
    val vbox1 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"vbox1" ~info:"GtkVBox" xmldata))
    method vbox1 = vbox1
    val hbox3 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox3" ~info:"GtkHBox" xmldata))
    method hbox3 = hbox3
    val label_20 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label_20" ~info:"GtkLabel" xmldata))
    method label_20 = label_20
    val graph_button =
      new GButton.button (GtkButton.Button.cast
        (Glade.get_widget_msg ~name:"graph_button" ~info:"GtkButton" xmldata))
    method graph_button = graph_button
    val hbox19 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox19" ~info:"GtkHBox" xmldata))
    method hbox19 = hbox19
    val graph_label =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"graph_label" ~info:"GtkLabel" xmldata))
    method graph_label = graph_label
    val image6 =
      new GMisc.image (GtkMisc.Image.cast
        (Glade.get_widget_msg ~name:"image6" ~info:"GtkImage" xmldata))
    method image6 = image6
    val btn_refresh_gr =
      new GButton.button (GtkButton.Button.cast
        (Glade.get_widget_msg ~name:"btn_refresh_gr" ~info:"GtkButton" xmldata))
    method btn_refresh_gr = btn_refresh_gr
    val image2 =
      new GMisc.image (GtkMisc.Image.cast
        (Glade.get_widget_msg ~name:"image2" ~info:"GtkImage" xmldata))
    method image2 = image2
    val vseparator4 =
      new GObj.widget_full (GtkMisc.Separator.cast
        (Glade.get_widget_msg ~name:"vseparator4" ~info:"GtkVSeparator" xmldata))
    method vseparator4 = vseparator4
    val label2 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label2" ~info:"GtkLabel" xmldata))
    method label2 = label2
    val grs_button =
      new GButton.button (GtkButton.Button.cast
        (Glade.get_widget_msg ~name:"grs_button" ~info:"GtkButton" xmldata))
    method grs_button = grs_button
    val hbox21 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox21" ~info:"GtkHBox" xmldata))
    method hbox21 = hbox21
    val grs_label =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"grs_label" ~info:"GtkLabel" xmldata))
    method grs_label = grs_label
    val image7 =
      new GMisc.image (GtkMisc.Image.cast
        (Glade.get_widget_msg ~name:"image7" ~info:"GtkImage" xmldata))
    method image7 = image7
    val btn_refresh_grs =
      new GButton.button (GtkButton.Button.cast
        (Glade.get_widget_msg ~name:"btn_refresh_grs" ~info:"GtkButton" xmldata))
    method btn_refresh_grs = btn_refresh_grs
    val image3 =
      new GMisc.image (GtkMisc.Image.cast
        (Glade.get_widget_msg ~name:"image3" ~info:"GtkImage" xmldata))
    method image3 = image3
    val vseparator6 =
      new GObj.widget_full (GtkMisc.Separator.cast
        (Glade.get_widget_msg ~name:"vseparator6" ~info:"GtkVSeparator" xmldata))
    method vseparator6 = vseparator6
    val label3 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label3" ~info:"GtkLabel" xmldata))
    method label3 = label3
    val strat =
      new GEdit.entry (GtkEdit.Entry.cast
        (Glade.get_widget_msg ~name:"strat" ~info:"GtkEntry" xmldata))
    method strat = strat
    val strat_list_viewport =
      new GBin.viewport (GtkBin.Viewport.cast
        (Glade.get_widget_msg ~name:"strat_list_viewport" ~info:"GtkViewport" xmldata))
    method strat_list_viewport = strat_list_viewport
    val vseparator7 =
      new GObj.widget_full (GtkMisc.Separator.cast
        (Glade.get_widget_msg ~name:"vseparator7" ~info:"GtkVSeparator" xmldata))
    method vseparator7 = vseparator7
    val btn_run =
      new GButton.button (GtkButton.Button.cast
        (Glade.get_widget_msg ~name:"btn_run" ~info:"GtkButton" xmldata))
    method btn_run = btn_run
    val run_box =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"run_box" ~info:"GtkHBox" xmldata))
    method run_box = run_box
    val label20 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label20" ~info:"GtkLabel" xmldata))
    method label20 = label20
    val image11 =
      new GMisc.image (GtkMisc.Image.cast
        (Glade.get_widget_msg ~name:"image11" ~info:"GtkImage" xmldata))
    method image11 = image11
    val synchronize =
      new GButton.toggle_button (GtkButton.ToggleButton.cast
        (Glade.get_widget_msg ~name:"synchronize" ~info:"GtkToggleButton" xmldata))
    method synchronize = synchronize
    val image10 =
      new GMisc.image (GtkMisc.Image.cast
        (Glade.get_widget_msg ~name:"image10" ~info:"GtkImage" xmldata))
    method image10 = image10
    val btn_preferences =
      new GButton.button (GtkButton.Button.cast
        (Glade.get_widget_msg ~name:"btn_preferences" ~info:"GtkButton" xmldata))
    method btn_preferences = btn_preferences
    val image1 =
      new GMisc.image (GtkMisc.Image.cast
        (Glade.get_widget_msg ~name:"image1" ~info:"GtkImage" xmldata))
    method image1 = image1
    val btn_leave_fullscreen =
      new GButton.button (GtkButton.Button.cast
        (Glade.get_widget_msg ~name:"btn_leave_fullscreen" ~info:"GtkButton" xmldata))
    method btn_leave_fullscreen = btn_leave_fullscreen
    val image5 =
      new GMisc.image (GtkMisc.Image.cast
        (Glade.get_widget_msg ~name:"image5" ~info:"GtkImage" xmldata))
    method image5 = image5
    val btn_enter_fullscreen =
      new GButton.button (GtkButton.Button.cast
        (Glade.get_widget_msg ~name:"btn_enter_fullscreen" ~info:"GtkButton" xmldata))
    method btn_enter_fullscreen = btn_enter_fullscreen
    val image4 =
      new GMisc.image (GtkMisc.Image.cast
        (Glade.get_widget_msg ~name:"image4" ~info:"GtkImage" xmldata))
    method image4 = image4
    val vpaned_corpus =
      new GPack.paned (GtkPack.Paned.cast
        (Glade.get_widget_msg ~name:"vpaned_corpus" ~info:"GtkHPaned" xmldata))
    method vpaned_corpus = vpaned_corpus
    val hbox10 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox10" ~info:"GtkHBox" xmldata))
    method hbox10 = hbox10
    val vbox_corpus =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"vbox_corpus" ~info:"GtkVBox" xmldata))
    method vbox_corpus = vbox_corpus
    val vbox6 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"vbox6" ~info:"GtkVBox" xmldata))
    method vbox6 = vbox6
    val viewport4 =
      new GBin.viewport (GtkBin.Viewport.cast
        (Glade.get_widget_msg ~name:"viewport4" ~info:"GtkViewport" xmldata))
    method viewport4 = viewport4
    val btn_show_corpus =
      new GButton.toggle_button (GtkButton.ToggleButton.cast
        (Glade.get_widget_msg ~name:"btn_show_corpus" ~info:"GtkToggleButton" xmldata))
    method btn_show_corpus = btn_show_corpus
    val corpus_label =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"corpus_label" ~info:"GtkLabel" xmldata))
    method corpus_label = corpus_label
    val viewport5 =
      new GBin.viewport (GtkBin.Viewport.cast
        (Glade.get_widget_msg ~name:"viewport5" ~info:"GtkViewport" xmldata))
    method viewport5 = viewport5
    val vpaned_left =
      new GPack.paned (GtkPack.Paned.cast
        (Glade.get_widget_msg ~name:"vpaned_left" ~info:"GtkHPaned" xmldata))
    method vpaned_left = vpaned_left
    val hbox2 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox2" ~info:"GtkHBox" xmldata))
    method hbox2 = hbox2
    val viewport1 =
      new GBin.viewport (GtkBin.Viewport.cast
        (Glade.get_widget_msg ~name:"viewport1" ~info:"GtkViewport" xmldata))
    method viewport1 = viewport1
    val vbox2 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"vbox2" ~info:"GtkVBox" xmldata))
    method vbox2 = vbox2
    val hbox4 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox4" ~info:"GtkHBox" xmldata))
    method hbox4 = hbox4
    val hbox5 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox5" ~info:"GtkHBox" xmldata))
    method hbox5 = hbox5
    val label5 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label5" ~info:"GtkLabel" xmldata))
    method label5 = label5
    val grs_zoom =
      new GRange.scale (GtkRange.Scale.cast
        (Glade.get_widget_msg ~name:"grs_zoom" ~info:"GtkHScale" xmldata))
    method grs_zoom = grs_zoom
    val label7 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label7" ~info:"GtkLabel" xmldata))
    method label7 = label7
    val hbox8 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox8" ~info:"GtkHBox" xmldata))
    method hbox8 = hbox8
    val grs_view =
      new GBin.scrolled_window (GtkBin.ScrolledWindow.cast
        (Glade.get_widget_msg ~name:"grs_view" ~info:"GtkScrolledWindow" xmldata))
    method grs_view = grs_view
    val vbox7 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"vbox7" ~info:"GtkVBox" xmldata))
    method vbox7 = vbox7
    val viewport6 =
      new GBin.viewport (GtkBin.Viewport.cast
        (Glade.get_widget_msg ~name:"viewport6" ~info:"GtkViewport" xmldata))
    method viewport6 = viewport6
    val btn_show_grs =
      new GButton.toggle_button (GtkButton.ToggleButton.cast
        (Glade.get_widget_msg ~name:"btn_show_grs" ~info:"GtkToggleButton" xmldata))
    method btn_show_grs = btn_show_grs
    val rewriting_label =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"rewriting_label" ~info:"GtkLabel" xmldata))
    method rewriting_label = rewriting_label
    val viewport7 =
      new GBin.viewport (GtkBin.Viewport.cast
        (Glade.get_widget_msg ~name:"viewport7" ~info:"GtkViewport" xmldata))
    method viewport7 = viewport7
    val vpaned_right =
      new GPack.paned (GtkPack.Paned.cast
        (Glade.get_widget_msg ~name:"vpaned_right" ~info:"GtkHPaned" xmldata))
    method vpaned_right = vpaned_right
    val hbox6 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox6" ~info:"GtkHBox" xmldata))
    method hbox6 = hbox6
    val viewport2 =
      new GBin.viewport (GtkBin.Viewport.cast
        (Glade.get_widget_msg ~name:"viewport2" ~info:"GtkViewport" xmldata))
    method viewport2 = viewport2
    val vbox4 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"vbox4" ~info:"GtkVBox" xmldata))
    method vbox4 = vbox4
    val hbox7 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox7" ~info:"GtkHBox" xmldata))
    method hbox7 = hbox7
    val hbox9 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox9" ~info:"GtkHBox" xmldata))
    method hbox9 = hbox9
    val label6 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label6" ~info:"GtkLabel" xmldata))
    method label6 = label6
    val module_zoom =
      new GRange.scale (GtkRange.Scale.cast
        (Glade.get_widget_msg ~name:"module_zoom" ~info:"GtkHScale" xmldata))
    method module_zoom = module_zoom
    val label12 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label12" ~info:"GtkLabel" xmldata))
    method label12 = label12
    val module_view =
      new GBin.scrolled_window (GtkBin.ScrolledWindow.cast
        (Glade.get_widget_msg ~name:"module_view" ~info:"GtkScrolledWindow" xmldata))
    method module_view = module_view
    val vbox8 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"vbox8" ~info:"GtkVBox" xmldata))
    method vbox8 = vbox8
    val viewport8 =
      new GBin.viewport (GtkBin.Viewport.cast
        (Glade.get_widget_msg ~name:"viewport8" ~info:"GtkViewport" xmldata))
    method viewport8 = viewport8
    val btn_show_module =
      new GButton.toggle_button (GtkButton.ToggleButton.cast
        (Glade.get_widget_msg ~name:"btn_show_module" ~info:"GtkToggleButton" xmldata))
    method btn_show_module = btn_show_module
    val label15 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label15" ~info:"GtkLabel" xmldata))
    method label15 = label15
    val viewport9 =
      new GBin.viewport (GtkBin.Viewport.cast
        (Glade.get_widget_msg ~name:"viewport9" ~info:"GtkViewport" xmldata))
    method viewport9 = viewport9
    val vpaned =
      new GPack.paned (GtkPack.Paned.cast
        (Glade.get_widget_msg ~name:"vpaned" ~info:"GtkVPaned" xmldata))
    method vpaned = vpaned
    val hbox13 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox13" ~info:"GtkHBox" xmldata))
    method hbox13 = hbox13
    val vbox3 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"vbox3" ~info:"GtkVBox" xmldata))
    method vbox3 = vbox3
    val label9 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label9" ~info:"GtkLabel" xmldata))
    method label9 = label9
    val graph_top_zoom =
      new GRange.scale (GtkRange.Scale.cast
        (Glade.get_widget_msg ~name:"graph_top_zoom" ~info:"GtkVScale" xmldata))
    method graph_top_zoom = graph_top_zoom
    val label8 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label8" ~info:"GtkLabel" xmldata))
    method label8 = label8
    val btn_gr_top_dep =
      new GButton.toggle_button (GtkButton.ToggleButton.cast
        (Glade.get_widget_msg ~name:"btn_gr_top_dep" ~info:"GtkToggleButton" xmldata))
    method btn_gr_top_dep = btn_gr_top_dep
    val btn_gr_top_dot =
      new GButton.toggle_button (GtkButton.ToggleButton.cast
        (Glade.get_widget_msg ~name:"btn_gr_top_dot" ~info:"GtkToggleButton" xmldata))
    method btn_gr_top_dot = btn_gr_top_dot
    val viewport10 =
      new GBin.viewport (GtkBin.Viewport.cast
        (Glade.get_widget_msg ~name:"viewport10" ~info:"GtkViewport" xmldata))
    method viewport10 = viewport10
    val graph_view_top =
      new GBin.scrolled_window (GtkBin.ScrolledWindow.cast
        (Glade.get_widget_msg ~name:"graph_view_top" ~info:"GtkScrolledWindow" xmldata))
    method graph_view_top = graph_view_top
    val hbox23 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox23" ~info:"GtkHBox" xmldata))
    method hbox23 = hbox23
    val vbox9 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"vbox9" ~info:"GtkVBox" xmldata))
    method vbox9 = vbox9
    val label10 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label10" ~info:"GtkLabel" xmldata))
    method label10 = label10
    val graph_bottom_zoom =
      new GRange.scale (GtkRange.Scale.cast
        (Glade.get_widget_msg ~name:"graph_bottom_zoom" ~info:"GtkVScale" xmldata))
    method graph_bottom_zoom = graph_bottom_zoom
    val label11 =
      new GMisc.label (GtkMisc.Label.cast
        (Glade.get_widget_msg ~name:"label11" ~info:"GtkLabel" xmldata))
    method label11 = label11
    val btn_gr_bottom_dep =
      new GButton.toggle_button (GtkButton.ToggleButton.cast
        (Glade.get_widget_msg ~name:"btn_gr_bottom_dep" ~info:"GtkToggleButton" xmldata))
    method btn_gr_bottom_dep = btn_gr_bottom_dep
    val btn_gr_bottom_dot =
      new GButton.toggle_button (GtkButton.ToggleButton.cast
        (Glade.get_widget_msg ~name:"btn_gr_bottom_dot" ~info:"GtkToggleButton" xmldata))
    method btn_gr_bottom_dot = btn_gr_bottom_dot
    val graph_view_bottom =
      new GBin.scrolled_window (GtkBin.ScrolledWindow.cast
        (Glade.get_widget_msg ~name:"graph_view_bottom" ~info:"GtkScrolledWindow" xmldata))
    method graph_view_bottom = graph_view_bottom
    val err_view_scroll =
      new GBin.scrolled_window (GtkBin.ScrolledWindow.cast
        (Glade.get_widget_msg ~name:"err_view_scroll" ~info:"GtkScrolledWindow" xmldata))
    method err_view_scroll = err_view_scroll
    val statusbar =
      new GMisc.statusbar (GtkMisc.Statusbar.cast
        (Glade.get_widget_msg ~name:"statusbar" ~info:"GtkStatusbar" xmldata))
    method statusbar = statusbar
    method reparent parent =
      vbox1#misc#reparent parent;
      toplevel#destroy ()
    method check_widgets () = ()
  end
class src_viewer ?(file="src/grew.glade") ?domain ?autoconnect(*=true*) () =
  let xmldata = Glade.create ~file  ~root:"src_viewer" ?domain () in
  object (self)
    inherit Glade.xml ?autoconnect xmldata
    val toplevel =
      new GWindow.window (GtkWindow.Window.cast
        (Glade.get_widget_msg ~name:"src_viewer" ~info:"GtkWindow" xmldata))
    method toplevel = toplevel
    val src_viewer =
      new GWindow.window (GtkWindow.Window.cast
        (Glade.get_widget_msg ~name:"src_viewer" ~info:"GtkWindow" xmldata))
    method src_viewer = src_viewer
    val vbox10 =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"vbox10" ~info:"GtkVBox" xmldata))
    method vbox10 = vbox10
    val scrolledwindow1 =
      new GBin.scrolled_window (GtkBin.ScrolledWindow.cast
        (Glade.get_widget_msg ~name:"scrolledwindow1" ~info:"GtkScrolledWindow" xmldata))
    method scrolledwindow1 = scrolledwindow1
    val source =
      new GText.view (GtkText.View.cast
        (Glade.get_widget_msg ~name:"source" ~info:"GtkTextView" xmldata))
    method source = source
    val close =
      new GButton.button (GtkButton.Button.cast
        (Glade.get_widget_msg ~name:"close" ~info:"GtkButton" xmldata))
    method close = close
    method reparent parent =
      vbox10#misc#reparent parent;
      toplevel#destroy ()
    method check_widgets () = ()
  end

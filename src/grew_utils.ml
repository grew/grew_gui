(***********************************************************************)
(*    Grew - a Graph Rewriting tool dedicated to NLP applications      *)
(*                                                                     *)
(*    Copyright 2011-2013 Inria, Université de Lorraine                *)
(*                                                                     *)
(*    Webpage: http://grew.fr                                          *)
(*    License: CeCILL (see LICENSE folder or "http://www.cecill.info") *)
(*    Authors: see AUTHORS file                                        *)
(***********************************************************************)

open Printf
open Log
open Conllx
open Libgrew
open Libamr

open Dep2pict

open Grew_args

(* ================================================================================ *)
module StringMap = Map.Make (String)

(* ================================================================================ *)
module String_ = struct
  let contains sub str =
    try let _ = Str.search_forward (Str.regexp_string sub) str 0 in true
    with Not_found -> false
end

(* ================================================================================ *)
module Array_ = struct
  (* dichotomic search in a sorted array *)
  let dicho_find elt array =
    let rec loop low high =
      (if low > high then raise Not_found);
      match (low+high)/2 with
      | middle when array.(middle) = elt -> middle
      | middle when array.(middle) < elt -> loop (middle+1) high
      | middle -> loop low (middle - 1) in
    loop 0 ((Array.length array) - 1)
end

(* ================================================================================ *)
module File = struct
  let read_lines file =
    let in_ch = open_in file in
    (* if the input file contains an UTF-8 byte order mark (EF BB BF), skip 3 bytes, else get back to 0 *)
    (match input_byte in_ch with 0xEF -> seek_in in_ch 3 | _ -> seek_in in_ch 0);

    let res = ref [] in
    try
      while true
      do res := (input_line in_ch) :: !res
      done; assert false
    with End_of_file -> close_in in_ch; List.rev !res

  let read file =
    let in_ch = open_in file in
    (* if the input file contains an UTF-8 byte order mark (EF BB BF), skip 3 bytes, else get back to 0 *)
    (match input_byte in_ch with 0xEF -> seek_in in_ch 3 | _ -> seek_in in_ch 0);

    let buff = Buffer.create 32 in
    try
      while true
      do bprintf buff "%s\n" (input_line in_ch);
      done; assert false
    with End_of_file -> Buffer.contents buff

  exception Found of int
  let get_suffix file_name =
    let len = String.length file_name in
    try
      for i = len-1 downto 0 do
        if file_name.[i] = '.'
        then raise (Found i)
      done;
      None
    with
    | Found i -> Some (String.sub file_name i (len-i))

end (* module File *)

(* ================================================================================ *)
module List_ = struct
  (** [index elt l] returns Some i where i is the index of the first occurence of [elt] in [l].
      None is returned if [l] does not contain [elt]. *)
  let index elt l =
    let rec loop i = function
      | [] -> None
      | e::t when e=elt -> Some i
      | _::t -> loop (i+1) t
    in loop 0 l

  let iteri fct =
    let rec loop i = function
      | [] -> ()
      | h::t -> (fct i h); (loop (i+1) t) in
    loop 0

  let mapi fct =
    let rec loop i = function
      | [] -> []
      | h::t -> let head = fct i h in head :: (loop (i+1) t)
    in loop 0

  let rec opt_map fct = function
    | [] -> []
    | h::t ->
      match (fct h) with
      | Some x -> x::(opt_map fct t)
      | None -> opt_map fct t

  let opt_mapi fct =
    let rec loop i = function
      | [] -> []
      | h::t ->
        match fct i h with
        | Some x -> x::(loop (i+1) t)
        | None -> loop (i+1) t
    in loop 0

end (* module List_ *)

(* ================================================================================ *)
module Pdf = struct
  let dot_to_file dot output_file =
    let temp_file_name,out_ch = Filename.open_temp_file ~mode:[Open_rdonly;Open_wronly;Open_text] "grew_" ".dot" in
    fprintf out_ch "%s" dot;
    close_out out_ch;
    ignore (Sys.command(sprintf "dot -Tpdf -o %s %s " output_file temp_file_name))
end (* module Pdf *)

(* ================================================================================ *)
module Svg = struct
  let dot_to_tmp dot =
    let temp_file_name,out_ch = Filename.open_temp_file ~mode:[Open_rdonly;Open_wronly;Open_text] "grew_" ".dot" in
    fprintf out_ch "%s" dot;
    close_out out_ch;
    let svg_file_name = Str.global_replace (Str.regexp ".dot") ".svg" temp_file_name in
    ignore (Sys.command(sprintf "dot -Tsvg -o %s %s " svg_file_name temp_file_name));
    svg_file_name

  let dot_to_file dot output_file =
    let temp_file_name,out_ch = Filename.open_temp_file ~mode:[Open_rdonly;Open_wronly;Open_text] "grew_" ".dot" in
    fprintf out_ch "%s" dot;
    close_out out_ch;
    ignore (Sys.command(sprintf "dot -Tsvg -o %s %s " output_file temp_file_name))

  let dep_to_tmp dep =
    let temp_file_name = Filename.temp_file "grew_" ".svg" in
    let d2p = Dep2pict.from_dep dep in
    let () = Dep2pict.save_svg ~filename:temp_file_name d2p in
    temp_file_name
end (* module Svg *)

(* ================================================================================ *)
module Corpus = struct

  exception Fail of string
  exception File_not_found of string

  let load_conll ?domain file =
    let conllx_corpus = Conllx_corpus.load ~config:!Grew_args.config file in
    Array.map (fun (sentid, conll) -> (sentid, Graph.of_grew_json (Conllx.to_json conll))) (Conllx_corpus.get_data conllx_corpus)

  let load_amr ?domain file =
    let amr_corpus = Amr_corpus.load file in
    Array.map (fun (sent_id,text,amr) ->
        try
          let gr = Amr.to_gr amr in
          let graph = Graph.of_gr ~config:!Grew_args.config ?domain gr in
          (sent_id, graph)
        with exc -> Log.fwarning "[id=%s] AMR skipped [exception: %s]" sent_id (Printexc.to_string exc); exit 1;
      ) amr_corpus

  let load_brown ?domain file =
    let lines = File.read_lines file in
    let brown_list =
      List_.opt_mapi
        (fun i line -> match Str.split (Str.regexp "#") line with
           | [] -> None
           | [line] -> let sentid = sprintf "%05d" i in Some (sentid, Graph.of_brown ~config:!Grew_args.config ?domain ~sentid line)
           | [sentid; line] -> Some (sentid, Graph.of_brown ~config:!Grew_args.config ?domain ~sentid line)
           | _ -> raise (Fail (sprintf "[file %s, line %d] Illegal Brown line >>>%s<<<<\n%!" file i line))
        ) lines in
    Array.of_list brown_list

  (** [load source] loads a corpus; [source] can be:
      - a folder, the corpus is the set of graphs (files matching *.gr or *.conll) in the folder
      - a conll file *)
  let get_graphs ?domain source =
    if not (Sys.file_exists source)
    then raise (File_not_found source);
    if Sys.is_directory source
    then (* if [source] is a folder *)
      begin
        let files_array = Sys.readdir source in
        let graph_list =
          Array.fold_right
            (fun file acc ->
               if Filename.check_suffix file ".gr"
               then (Filename.chop_extension file, Graph.load ~config:!Grew_args.config ?domain (Filename.concat source file)) :: acc
               else if (Filename.check_suffix file ".conll" || Filename.check_suffix file ".conllu")
               then
                 let conllx = Conllx.of_string (File.read (Filename.concat source file)) in
                 let graph = Graph.of_grew_json (Conllx.to_json conllx) in
                 match Conllx.get_sent_id_opt conllx with
                 | Some sentid -> (sentid, graph) :: acc
                 | None -> (file, graph) :: acc
               else acc
            ) files_array [] in
        Array.of_list graph_list
      end
    else (* if [source] is a file *)
      match File.get_suffix source with
      | Some s when String_.contains "conll" s -> load_conll ?domain source
      | Some s when String_.contains "melt" s -> load_brown ?domain source
      | Some s when String_.contains "brown" s -> load_brown ?domain source
      | Some s when String_.contains "amr" s -> load_amr ?domain source
      | Some s when String_.contains "gr" s -> [| (source, Graph.load ?domain ~config:!Grew_args.config source) |]
      | _ ->
        Log.fwarning "Unknown suffix for file \"%s\", trying to guess format..." source;
        try load_conll ?domain source
        with _ ->
        try load_brown ?domain source
        with _ ->
        try load_amr ?domain source
        with _ -> raise (Fail (sprintf "Cannot load file \"%s\", unknown format" source))
end (* module Corpus *)

(* ==================================================================================================== *)
module Int_map = Map.Make (Int)

(* ==================================================================================================== *)
module Timer = struct
  type t = int

  let cpt = ref 0

  let table = ref Int_map.empty

  let create () =
    incr cpt;
    let current_time = Unix.times () in
    table := Int_map.add !cpt current_time.Unix.tms_utime !table;
    !cpt

  let see timer =
    let current_time = Unix.times () in
    current_time.Unix.tms_utime -. (Int_map.find timer !table)

  let get timer =
    let current_time = Unix.times () in
    let diff = current_time.Unix.tms_utime -. (Int_map.find timer !table) in
    table := Int_map.remove !cpt !table;
    diff
end

(***********************************************************************)
(*    Grew - a Graph Rewriting tool dedicated to NLP applications      *)
(*                                                                     *)
(*    Copyright 2011-2013 Inria, Université de Lorraine                *)
(*                                                                     *)
(*    Webpage: http://grew.fr                                          *)
(*    License: CeCILL (see LICENSE folder or "http://www.cecill.info") *)
(*    Authors: see AUTHORS file                                        *)
(***********************************************************************)

open Arg
open Log
open Conllx
open Libgrew

module Grew_args = struct

  let grs = ref None
  let fullscreen = ref false

  let input_data = ref None
  let strat = ref "main"
  let features = ref None
  let main_feat = ref None
  let timeout = ref None
  let config = ref (Conllx_config.build "ud")

  let usage =
    "grew-gui: a Gtk interface\n"^
    "\n"^
    "Options:"

  let dump_version () =
    Printf.printf "grew:    %s\n" VERSION;
    Printf.printf "libgrew: %s\n" (Libgrew.get_version ())

  let obsolote mode = failwith (Printf.sprintf "The mode %s was removed, sorry!" mode)

  let args = [
    (* options for all modes *)
    "-grs", String (fun s -> grs := Some s),          "<grs_file>              chose the grs file to load";
    "-strat", String (fun s -> strat := s),                   "<strat>               set the module strategy to use";
    "-seq", String (fun s -> strat := s),                   "<strat>                 [DEPRECATED] replaced by -strat option";
    "-timeout", Float (fun f -> timeout := Some f; Rewrite.set_timeout (Some f)),                   "<float>             set a timeout on rewriting";
    "-max_rules", Int (fun v -> Rewrite.set_max_rules v),                   "<int>             set the maximum steps of rewriting (default: 10000)";
    "-features", String (fun s -> features := Some (Str.split (Str.regexp "; *") s)),            "<feat_name_list>   set the list of feature names to printf in dep format";
    "-main_feat", String (fun s -> main_feat := Some s),       "<feat_name_list>  set the list of feature names used in dep format to set the \"main word\"";
    "-safe_commands", Unit (fun () -> Libgrew.set_safe_commands true),  "              enable safe_commands mode";
    "-debug", Unit (fun () -> Libgrew.set_debug_mode true),  "                      enable debug mode";
    "-config", String (fun s -> config := Conllx_config.build s),  "                      set config";
    "-fullscreen", Unit (fun () -> fullscreen := true), "                 fullscreen";
    "-i", String (fun file -> input_data := Some file),  "<input_data>              set the input data (file or directory) where to find graph files (.gr or .conll) in corpus or det mode";
  ]

  let parse () =
    Arg.parse args (fun s -> Printf.printf "%s" s) usage

end
